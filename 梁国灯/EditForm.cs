﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SqlInterface
{
    public partial class EditForm : Form
    {
        public EditForm()
        {
            InitializeComponent();
        }

        private int userId;
        //private string userName;
        //private string userPwd;
        //private string userSex;
        //private int userAge;

        public EditForm(int userId, string userName, string userPwd, string userSex, int userAge)
        {
            InitializeComponent();
            this.userId = userId;
            this.userName.Text = userName;
            this.userPwd.Text = userPwd;
            this.userSex.Text = userSex;
            this.userAge.Text = userAge.ToString();
        }

        private void EditForm_Load(object sender, EventArgs e)
        {

        }
        
        //保存
        private void button1_Click(object sender, EventArgs e)
        {
            var userName = this.userName.Text;
            var userPwd = this.userPwd.Text;
            var userSex = this.userSex.Text;
            var userAge = this.userAge.Text;
            //利用id判断正在进行的是添加还是更新操作
            if (this.userId > 0)
            {
                //更新
                string update = "UPDATE userInfo "
                            + "SET userName = '"+userName+"' , userPwd = '"+userPwd+"' , userSex = '"+userSex+"' , userAge = '"+userAge+"' "
                            + "WHERE userId = "+this.userId;
                DBHelper.EditData(update);
                MessageBox.Show("更新成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                //添加
                string add = "INSERT INTO userInfo "
                            +"VALUES('"+userName+"' , '"+userPwd+"' , '"+userSex+"' , "+userAge+")";
                DBHelper.EditData(add);
                MessageBox.Show("添加成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            this.DialogResult = DialogResult.Yes; //操作完成后令对话框结果返回为Yes
            this.Close();
        }

        //取消
        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No; //取消后令对话框结果返回为No
            this.Close();
        }

    }
}
